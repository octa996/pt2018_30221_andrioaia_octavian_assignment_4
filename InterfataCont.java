import java.util.ArrayList;

import javax.swing.*;

public class InterfataCont extends JFrame {

	
	JTextField tiban,tid,tbalanta;
	JTextField tiban1,tid1,tbalanta1,tdobanda;
	
	String iban,id,balanta,dobanda;
	Cont c;
	String delcnp,deliban;
	JTextField tdelcnp,tdeliban;
	String[] coloana2=
		{
			"iban","cnpPersoana","balanta"	
		};
	JTable tabel;
	public InterfataCont(Banca b)
	{
		JFrame jf=new JFrame();
		JPanel p=new JPanel();
		JPanel jbut=new JPanel();
		
		
		JButton add=new JButton("Adaugare cont cheltuieli");
		JButton add1=new JButton("Adaugare cont economii");
		JButton del=new JButton("Stergere cont");
		JButton afis=new JButton("Afsare conturi");
		JButton depozit=new JButton("Depozitare");
		JButton retragere=new JButton("Retragere");
		
		JButton aplicare=new JButton("Dobanda pentru Economii");
		
		JLabel jdobanda=new JLabel("Introduceti dobanda");
		tdobanda=new JTextField("",15);
		
		
		
		
		
		JPanel pdobanda=new JPanel();
		pdobanda.add(jdobanda);
		pdobanda.add(tdobanda);
		
		
		
		jbut.add(add);
		jbut.add(add1);
		jbut.add(del);
		jbut.add(afis);
		jbut.add(depozit);
		jbut.add(retragere);
		jbut.add(aplicare);
		JLabel l0=new JLabel("Cont cheltuieli");
		
		JLabel l1=new JLabel("Introduceti iban-ul");
		tiban=new JTextField("",15);
		
		JLabel l2=new JLabel("Introduceti id-ul");
		tid=new JTextField("",15);
		
		JLabel l3=new JLabel("Introduceti balanta");
		tbalanta=new JTextField("",15);
		
		JPanel p4=new JPanel();
		
		JLabel l01=new JLabel("Cont economii");
		
		JLabel l11=new JLabel("Introduceti iban-ul");
		tiban1=new JTextField("",15);
		
		JLabel l22=new JLabel("Introduceti id-ul");
		tid1=new JTextField("",15);
		
		JLabel l33=new JLabel("Introduceti balanta");
		tbalanta1=new JTextField("",15);
		
		
		p4.add(l01);
		p4.add(l11);
		p4.add(tiban1);
		p4.add(l22);
		p4.add(tid1);
		p4.add(l33);
		p4.add(tbalanta1);

		
		
		
		
		JPanel p1=new JPanel();
		p1.add(l0);
		p1.add(l1);
		p1.add(tiban);
		p1.add(l2);
		p1.add(tid);
		p1.add(l3);
		p1.add(tbalanta);
		
		JPanel p2=new JPanel();
		JPanel p3=new JPanel();
		
		JLabel del1=new JLabel("Introduceti cnp-u de unde stergem");
		tdelcnp=new JTextField("",15);
		
		JLabel del2=new JLabel("Introduceti iban-ul contului care va fi sters");
		tdeliban=new JTextField("",15);
		
		
		p3.add(del1);
		p3.add(tdelcnp);
		
		p3.add(del2);
		p3.add(tdeliban);
		
		
		
	
		
			
		p.add(p1);
		p.add(p4);
		p.add(p2);
		p.add(jbut);
		p.add(p3);
		p.add(pdobanda);
		
		
		add1.addActionListener((e)->
		{
			actiune4(b);
		});
			
		add.addActionListener((e)->
		{
			actiune1(b);
			
		});
		
		
		afis.addActionListener((e)->
		{
			actiune2(b);
			
		});
		
		
		del.addActionListener((e)->
		{
			actiune3(b);
			
		});
	
		depozit.addActionListener((e)->
		{
			actiune5(b);
			
			
		});
		
		retragere.addActionListener((e)->
		{
			actiune6(b);
			
		});
		
		aplicare.addActionListener((e)->
		{
			actiune7(b);
			
		});

		jf.add(p);
		 jf.setSize(800, 200);
		    jf.setVisible(true);
		    jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	

	private void actiune7(Banca b) {
		// TODO Auto-generated method stub
		dobanda=tdobanda.getText();
		b.aplicareDobanda(Integer.parseInt(dobanda));
	}



	private void actiune6(Banca b) {
		// TODO Auto-generated method stub
		new Retragere(b);
		
	}



	private void actiune5(Banca b) {
		// TODO Auto-generated method stub
		new Depozit(b);
		
	}



	private void actiune4(Banca b) {
		// TODO Auto-generated method stub
		
		iban=tiban1.getText();
		id=tid1.getText();
		balanta=tbalanta1.getText();
		c=new ContEconomii(iban,Integer.parseInt(id),Integer.parseInt(balanta));
		Persoana p=new Persoana(1961224,"Andrioaia","Octavian",21,"Clabucet");
		b.adaugaCont(c,p);
		
	}

	private void actiune3(Banca b) {
		// TODO Auto-generated method stub
		
		
		delcnp=tdelcnp.getText();
		deliban=tdeliban.getText();
		b.stergeCont(Integer.parseInt(delcnp), deliban);
		
		
		
	}

	private void actiune1(Banca b) {
		// TODO Auto-generated method stub
	iban=tiban.getText();
	id=tid.getText();
	balanta=tbalanta.getText();
	c=new ContCheltuieli(iban,Integer.parseInt(id),Integer.parseInt(balanta));
	Persoana p=new Persoana(1,"Andrioaia","Octavian",21,"Clabucet");
	b.adaugaCont(c,p);
		
	}

	private void actiune2(Banca b) {
		// TODO Auto-generated method stub
		ArrayList<Cont> lista=b.afisareCont();
		Object data[][] = new Object[1000][1000];
		for(int i=0;i<lista.size();i++)
		{
			data[i][0]=lista.get(i).getIBAN();
			data[i][1]=lista.get(i).getIdPersoana();
			data[i][2]=lista.get(i).getBalanta();
		}
		
		tabel=new JTable(data,coloana2);
		JScrollPane sc=new JScrollPane(tabel);
		
		JFrame f=new JFrame();
		f.add(tabel);
		f.setVisible(true);
	}
	
	
}
