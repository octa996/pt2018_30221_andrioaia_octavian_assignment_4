

public class Persoana {
	
	
	public int cnp;
	public String nume;
	public String prenume;
	public int varsta;
	public String adresa;
	
	
	public Persoana(int cnp,String nume, String prenume, int varsta, String adresa) {
		super();
		this.cnp=cnp;
		this.nume = nume;
		this.prenume = prenume;
		this.varsta = varsta;
		this.adresa = adresa;
	}
	
	public int getCNP()
	{
		return cnp;
	}
	
	public void setCNP(int cnp)
	{
		this.cnp=cnp;
	}
	
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getPrenume() {
		return prenume;
	}
	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}
	public int getVarsta() {
		return varsta;
	}
	public void setVarsta(int varsta) {
		this.varsta = varsta;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	
	
	
	

}
