import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Banca {
	
	HashMap<Persoana,Set<Cont>> bank;
	ArrayList<Persoana> persoane=new ArrayList<Persoana>();
	ArrayList<Cont>  conturi=new ArrayList<Cont>();

	
	
	public Banca()
	{
	bank = new HashMap<Persoana, Set<Cont>>();
	}
	
	
	void adaugaClient(Persoana p1)
	{
	
		bank.put(p1, new HashSet<Cont>());
		persoane.add(p1);
		
		
	
	}
	
	
	
	ArrayList<Cont> afisareCont()
	{
		return conturi;
	}
	ArrayList<Persoana> afisarePersoane()
	{
		return persoane;
	}
	
	void adaugaCont(Cont c1,Persoana p1)
	{
		
		bank.get(p1).add(c1);

		conturi.add(c1);
	}
	void stergePersoana(int cnp)
	{
		for(Persoana p:bank.keySet())
		{
			if(p.getCNP()==cnp)
			{
				bank.remove(p);
				persoane.remove(p);
			}
		}
		
	}
	
	
	
	void stergeCont(int cnp,String iban)
	{
		for(Persoana p:bank.keySet())
		{
			if(p.getCNP()==cnp)
			{
				for(Set<Cont> s:bank.values())
				{
					
					for(Cont c :s)
					{
						
						if(iban.equals(c.getIBAN())==true)
						{
							s.remove(c);
							conturi.remove(c);
						}
						
					}
			}
		}
	}
	}
	
	void editClient(int cnp,String nume,String adresa)
	{
		for(Persoana p:bank.keySet())
		{
			if(p.getCNP()==cnp)
			{
				p.setNume(nume);
				p.setAdresa(adresa);
			}
						
		}
	}
	
	void depunere (String iban,int sum)
	{
		for(Persoana p:bank.keySet())
		{
				for(Set<Cont> s:bank.values())
				{
					
					for(Cont c :s)
					{
						
						if(iban.equals(c.getIBAN())==true)
						{
							c.depunere(sum);
						}
						
					}
			}
		}
	}


	public void aplicareDobanda(int procent)
	{
		for(Persoana p:bank.keySet())
		{
				for(Set<Cont> s:bank.values())
				{
					
					for(Cont c :s)
					{
						
						c.aplicareDobanda(procent);
						
					}
			}
		}
	}
	
	public void retragere(String iban, int sum) {
		// TODO Auto-generated method stub
		for(Persoana p:bank.keySet())
		{
				for(Set<Cont> s:bank.values())
				{
					
					for(Cont c :s)
					{
						
						if(iban.equals(c.getIBAN())==true)
						{
							c.retragere(sum);
						}
						
					}
			}
		}
	}
	}
	
